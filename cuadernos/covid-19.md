---
title: "Covid-19"
author: "José Miguel"
date: "31/05/2020"
output: github_document
---

# Introducción

Este [virus](https://en.wikipedia.org/wiki/Coronavirus_disease_2019) que se ha convertido en una pandemia global del siglo XXI y cuyo impacto en nuestras vidas está aún por determinar, nos servirá como excusa para ilustrar cómo usar [R](https://www.r-project.org/) para analizar y visualizar datos abiertos relativos al covid-19.

## Fuentes de datos

Agradecer el trabajo realizado por las siguientes personas y organizaciones que han facilitado datos listos para ser analizados:

- [Our World In Data](https://www.ourworldindata.org)
- [Datadista](https://github.com/datadista/datasets/tree/master/COVID%2019) para analizar por comunidad autónoma española.

## Dependencias

Para realizar las operaciones pertinentes usaremos las bibliotecas proporcionadas por [tidyverse](https://www.tidyverse.org/), que ya tenemos instaladas.

También usaremos [knitr](https://yihui.org/knitr/) para embellecer las tablas.



## Situación global

Importamos los datos proporcionados por Our World In Data.



Para ver el dataframe, descomenta la siguiente línea:


```r
#covid19_df
```

> El carácter '#' se usa para indicar un comentario.

Estamos interesados en la siguiente información:

- país o *location*
- casos por día o *new_cases*
- defunciones por día o *new_deaths*

Vamos a extraer el total de casos y defunciones por país para el último día disponible. Y el último día disponible lo obtenemos de la siguiente forma:


> Realmente considero el penúltimo día porque algunos países tardan en reportar :(


```r
#last_day <- "2020-05-28"
last_day <- last_date$max[1]
last_day_df <- filter(covid19_df, date == last_day)
```

Seleccionemos los diez países con más casos registrados, descartando la entrada para el total mundial:


|date       |location       | total_cases| total_deaths|
|:----------|:--------------|-----------:|------------:|
|2020-05-30 |United States  |     1747087|       102836|
|2020-05-30 |Brazil         |      465166|        27878|
|2020-05-30 |Russia         |      387623|         4374|
|2020-05-30 |United Kingdom |      271222|        38161|
|2020-05-30 |Spain          |      239228|        27125|
|2020-05-30 |Italy          |      232248|        33229|
|2020-05-30 |Germany        |      181196|         8489|
|2020-05-30 |India          |      173763|         4971|
|2020-05-30 |Turkey         |      162120|         4489|
|2020-05-30 |France         |      149668|        28714|


```r
p <- ggplot(data = top10_cases, 
            mapping = aes(x = reorder(location, -total_cases), y = total_cases ))+ 
  geom_col()
caption <- str_interp("Total de casos a día ${last_day}")
p + guides(x = guide_axis(order = 1, angle = 45)) + 
  labs(x = "País",
       y = "Casos",
       caption = caption)
```

![plot of chunk total_cases](figure/total_cases-1.png)

La segunda instrucción es para ordenar los países por número decreciente de casos.


```r
p2 <- ggplot(data = top10_cases, 
             mapping = aes(x = reorder(location, -total_deaths), y = total_deaths )) + 
  geom_col()
caption <- str_interp("Total de muertes a día ${last_day}")
p2 + guides(x = guide_axis(angle = 45)) + 
  labs(x = "País",
       y = "Fallecimientos",
       caption = caption)
```

![plot of chunk total_deaths](figure/total_deaths-1.png)

Finalmente vamos a representar el primer gráfico, asignando una escala de colores por número de fallecimientos.


```r
p3 <- ggplot(data = top10_cases, 
             mapping = aes(x = reorder(location, -total_deaths), 
                           y = total_cases, fill = total_deaths )) + geom_col()
caption <- str_interp("Total de casos y muertes a día ${last_day}")
p3 + guides(x = guide_axis(angle = 45)) + 
  labs(x = "País",
       y = "Casos",
       caption = caption)
```

![plot of chunk total_cases_deaths](figure/total_cases_deaths-1.png)

Opcionalmente podemos guardar el gráfico generado en un fichero, pero debes descomentar el comando.


```r
#ggsave("top10_cases_deaths.png", width=10, height=5)
```

El problema de la última gráfica es que puede ser un poco complicada de analizar. Quizás mejor representar casos y fallecimientos en dos ejes:


```r
p4 <- ggplot(data = top10_cases, 
             mapping = aes(x = total_deaths, y = total_cases, label = location )) + geom_label()
caption <- str_interp("A día ${last_day}")
p4 + labs(label = "Countries", 
          x = "Fallecimientos",
          y = "Casos",
          caption = caption)
```

![plot of chunk cases_deaths_total](figure/cases_deaths_total-1.png)
