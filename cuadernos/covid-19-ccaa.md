---
title: "Covid-19 CC.AA"
author: "José Miguel"
date: "31/05/2020"
output: github_document
---

## Introducción

En este documente realizaremos un estudio de la incidencia del COVID-19 por comunidad autónoma española.

Agradecer el trabajo realizado por Datadista que ha facilitado datos listos para ser analizados:

- [Datadista](https://github.com/datadista/datasets/tree/master/COVID%2019) para analizar por comunidad autónoma española.



## Datos

Cargamos el conjunto de bibliotecas tidyverse:



y leemos los datos en formato csv facilitados por Datadista:



Vamos a centrarnos en los datos del último día:




```r
#last_day <- "2020-04-22"
last_day <- last_date$max[1]
last_day_df <- filter(covid19_df, Fecha == last_day)
```

Ordenemos por comunidad autónoma según el número de hospitalizados:



> Usamos paréntesis a ambos lados de la expresión para ver el resultado, y ahorrarnos invocar la > variable en una segunda línea.


```r
p1 <- ggplot(data = hospitalizados, mapping = aes(x = reorder(CCAA, -Hospitalizados), y = Hospitalizados )) + geom_col()
caption <- str_interp("Día ${last_day}")
p1 + labs(caption = caption, 
          x = "Comunidad autónoma") + 
  guides(x = guide_axis(order = 1, angle = 45))
```

![plot of chunk hospitalizados](figure/hospitalizados-1.png)

Pásemos a continuación a visualizar la relación entre fallecidos y recuperados.


```r
p2 <- ggplot(data = hospitalizados, mapping = aes(x = Fallecidos, y = Hospitalizados, label = CCAA )) + geom_label()
caption <- str_interp("Día ${last_day}")
p2 + labs(caption = caption)
```

![plot of chunk fallecidos_recuperados](figure/fallecidos_recuperados-1.png)

## Comentarios sobre gráficas

Como estos cuadernos son documentos dinámicos, hubiéramos comenzado explorando la gráfica como se muestra a continuación, y para variar nos fijaremos en la dimensión o columna de cuidados intensivos(UCI).


```r
ggplot(data = hospitalizados, mapping = aes(x = CCAA, y = UCI)) + geom_col()
```

![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-2-1.png)

Como el resultado es francamente mejorable hacemos uso de la [documentación](https://ggplot2.tidyverse.org/reference/index.html) que nos indica cómo ordenar las comunidades autónomas por orden decreciente de ingresados en UCI en vez del comportamiento por defecto de ordenarlas alfabéticamente. También por cuestiones de legibilidad las inclinamos cuarenta y cinco grados.
