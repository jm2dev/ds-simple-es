---
title: "COVID 19 Región de Murcia"
author: "José Miguel"
date: "31/05/2020"
output: github_document
---

## Introducción

Situación en la Región de Murcia de afectados por COVID-19.

Existe un [tablón information](https://comunidadcovid.maps.arcgis.com/apps/opsdashboard/index.html#/e2fd08a889354d0d996362d8bd22d397) y un [resumen](http://www.murciasalud.es/pagina.php?id=458440), pero nosotros estamos interesados en los datos puros y duros.

Como viene siendo habitual en esta serie recurriremos a los datos facilitados por [datadista](https://datadista.com/).

## Datos

Cargamos las bibliotecas *tidyverse*





y leemos los datos en formato csv facilitados por Datadista:




```r
covid19_mur_df <- filter(covid19_df, CCAA == "Murcia")
```

Allí donde no tenemos datos (NA) para nuestras columnas de interés, insertaremos un 0.


```r
covid19_rm_df <- covid19_mur_df %>%
  replace_na(list(Casos = 0, Fallecidos = 0, Hospitalizados = 0))
```


```r
kable(covid19_rm_df)
```

<table>
 <thead>
  <tr>
   <th style="text-align:left;"> Fecha </th>
   <th style="text-align:left;"> cod_ine </th>
   <th style="text-align:left;"> CCAA </th>
   <th style="text-align:right;"> Casos </th>
   <th style="text-align:right;"> PCR+ </th>
   <th style="text-align:right;"> TestAc+ </th>
   <th style="text-align:right;"> Hospitalizados </th>
   <th style="text-align:right;"> UCI </th>
   <th style="text-align:right;"> Fallecidos </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> 2020-02-20 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-02-21 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-02-22 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-02-23 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-02-24 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-02-25 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-02-26 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-02-27 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-02-28 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-02-29 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-01 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-02 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-03 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-04 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-05 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-06 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-07 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-08 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-09 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-10 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-11 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-12 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-13 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-14 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 71 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-15 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 77 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-16 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-17 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 122 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-18 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 167 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-19 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 204 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-20 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 240 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 61 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-21 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 296 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 78 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-22 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 345 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 80 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-23 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 385 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 99 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-24 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 477 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 127 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-25 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 596 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 172 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 8 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-26 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 714 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 209 </td>
   <td style="text-align:right;"> 53 </td>
   <td style="text-align:right;"> 15 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-27 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 802 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 231 </td>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:right;"> 17 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-28 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 872 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 260 </td>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:right;"> 20 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-29 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 939 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 265 </td>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:right;"> 25 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-30 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 974 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 283 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 34 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-03-31 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1041 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 304 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 37 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-01 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1084 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 389 </td>
   <td style="text-align:right;"> 68 </td>
   <td style="text-align:right;"> 42 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-02 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1145 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 405 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 46 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-03 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1188 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 434 </td>
   <td style="text-align:right;"> 79 </td>
   <td style="text-align:right;"> 51 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-04 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1235 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 447 </td>
   <td style="text-align:right;"> 80 </td>
   <td style="text-align:right;"> 59 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-05 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1259 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 468 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 68 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-06 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1259 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 500 </td>
   <td style="text-align:right;"> 84 </td>
   <td style="text-align:right;"> 78 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-07 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1297 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 515 </td>
   <td style="text-align:right;"> 86 </td>
   <td style="text-align:right;"> 85 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-08 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1322 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 533 </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 88 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-09 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1338 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:right;"> 533 </td>
   <td style="text-align:right;"> 88 </td>
   <td style="text-align:right;"> 90 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-10 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1371 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 533 </td>
   <td style="text-align:right;"> 88 </td>
   <td style="text-align:right;"> 94 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-11 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1380 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:right;"> 537 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 97 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-12 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1387 </td>
   <td style="text-align:right;"> 65 </td>
   <td style="text-align:right;"> 543 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 101 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-13 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1393 </td>
   <td style="text-align:right;"> 86 </td>
   <td style="text-align:right;"> 566 </td>
   <td style="text-align:right;"> 96 </td>
   <td style="text-align:right;"> 106 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-14 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1400 </td>
   <td style="text-align:right;"> 128 </td>
   <td style="text-align:right;"> 574 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 109 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-15 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1416 </td>
   <td style="text-align:right;"> 174 </td>
   <td style="text-align:right;"> 575 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 111 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-16 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1424 </td>
   <td style="text-align:right;"> 202 </td>
   <td style="text-align:right;"> 585 </td>
   <td style="text-align:right;"> 99 </td>
   <td style="text-align:right;"> 112 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-17 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1430 </td>
   <td style="text-align:right;"> 217 </td>
   <td style="text-align:right;"> 611 </td>
   <td style="text-align:right;"> 102 </td>
   <td style="text-align:right;"> 115 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-18 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1438 </td>
   <td style="text-align:right;"> 215 </td>
   <td style="text-align:right;"> 612 </td>
   <td style="text-align:right;"> 102 </td>
   <td style="text-align:right;"> 116 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-19 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1440 </td>
   <td style="text-align:right;"> 219 </td>
   <td style="text-align:right;"> 612 </td>
   <td style="text-align:right;"> 103 </td>
   <td style="text-align:right;"> 117 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-20 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1445 </td>
   <td style="text-align:right;"> 251 </td>
   <td style="text-align:right;"> 614 </td>
   <td style="text-align:right;"> 104 </td>
   <td style="text-align:right;"> 120 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-21 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1455 </td>
   <td style="text-align:right;"> 262 </td>
   <td style="text-align:right;"> 622 </td>
   <td style="text-align:right;"> 104 </td>
   <td style="text-align:right;"> 123 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-22 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1460 </td>
   <td style="text-align:right;"> 261 </td>
   <td style="text-align:right;"> 624 </td>
   <td style="text-align:right;"> 104 </td>
   <td style="text-align:right;"> 124 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-23 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1468 </td>
   <td style="text-align:right;"> 273 </td>
   <td style="text-align:right;"> 625 </td>
   <td style="text-align:right;"> 105 </td>
   <td style="text-align:right;"> 125 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-24 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1468 </td>
   <td style="text-align:right;"> 288 </td>
   <td style="text-align:right;"> 625 </td>
   <td style="text-align:right;"> 105 </td>
   <td style="text-align:right;"> 126 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-25 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1474 </td>
   <td style="text-align:right;"> 297 </td>
   <td style="text-align:right;"> 627 </td>
   <td style="text-align:right;"> 106 </td>
   <td style="text-align:right;"> 127 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-26 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1474 </td>
   <td style="text-align:right;"> 297 </td>
   <td style="text-align:right;"> 627 </td>
   <td style="text-align:right;"> 106 </td>
   <td style="text-align:right;"> 128 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-27 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1475 </td>
   <td style="text-align:right;"> 318 </td>
   <td style="text-align:right;"> 629 </td>
   <td style="text-align:right;"> 106 </td>
   <td style="text-align:right;"> 130 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-28 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1480 </td>
   <td style="text-align:right;"> 360 </td>
   <td style="text-align:right;"> 635 </td>
   <td style="text-align:right;"> 106 </td>
   <td style="text-align:right;"> 130 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-29 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1486 </td>
   <td style="text-align:right;"> 408 </td>
   <td style="text-align:right;"> 637 </td>
   <td style="text-align:right;"> 108 </td>
   <td style="text-align:right;"> 130 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-04-30 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1488 </td>
   <td style="text-align:right;"> 472 </td>
   <td style="text-align:right;"> 638 </td>
   <td style="text-align:right;"> 109 </td>
   <td style="text-align:right;"> 132 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-05-01 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1491 </td>
   <td style="text-align:right;"> 475 </td>
   <td style="text-align:right;"> 639 </td>
   <td style="text-align:right;"> 110 </td>
   <td style="text-align:right;"> 132 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-05-02 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1492 </td>
   <td style="text-align:right;"> 482 </td>
   <td style="text-align:right;"> 641 </td>
   <td style="text-align:right;"> 110 </td>
   <td style="text-align:right;"> 134 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-05-03 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1492 </td>
   <td style="text-align:right;"> 482 </td>
   <td style="text-align:right;"> 645 </td>
   <td style="text-align:right;"> 110 </td>
   <td style="text-align:right;"> 134 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-05-04 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1496 </td>
   <td style="text-align:right;"> 555 </td>
   <td style="text-align:right;"> 650 </td>
   <td style="text-align:right;"> 111 </td>
   <td style="text-align:right;"> 136 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-05-05 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1498 </td>
   <td style="text-align:right;"> 635 </td>
   <td style="text-align:right;"> 650 </td>
   <td style="text-align:right;"> 111 </td>
   <td style="text-align:right;"> 136 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-05-06 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1501 </td>
   <td style="text-align:right;"> 691 </td>
   <td style="text-align:right;"> 652 </td>
   <td style="text-align:right;"> 111 </td>
   <td style="text-align:right;"> 137 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-05-07 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1504 </td>
   <td style="text-align:right;"> 748 </td>
   <td style="text-align:right;"> 659 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 137 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-05-08 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1506 </td>
   <td style="text-align:right;"> 757 </td>
   <td style="text-align:right;"> 662 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 138 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-05-09 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1508 </td>
   <td style="text-align:right;"> 821 </td>
   <td style="text-align:right;"> 662 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 139 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-05-10 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1517 </td>
   <td style="text-align:right;"> 815 </td>
   <td style="text-align:right;"> 665 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 139 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-05-11 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1517 </td>
   <td style="text-align:right;"> 816 </td>
   <td style="text-align:right;"> 665 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 139 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-05-12 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1525 </td>
   <td style="text-align:right;"> 873 </td>
   <td style="text-align:right;"> 666 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 142 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-05-13 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1532 </td>
   <td style="text-align:right;"> 905 </td>
   <td style="text-align:right;"> 672 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 142 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-05-14 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1534 </td>
   <td style="text-align:right;"> 943 </td>
   <td style="text-align:right;"> 675 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 143 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-05-15 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1542 </td>
   <td style="text-align:right;"> 1004 </td>
   <td style="text-align:right;"> 676 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 144 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-05-16 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1551 </td>
   <td style="text-align:right;"> 1015 </td>
   <td style="text-align:right;"> 676 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 144 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-05-17 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1556 </td>
   <td style="text-align:right;"> 1039 </td>
   <td style="text-align:right;"> 678 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 145 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-05-18 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1558 </td>
   <td style="text-align:right;"> 1039 </td>
   <td style="text-align:right;"> 679 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 145 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-05-19 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1560 </td>
   <td style="text-align:right;"> 1039 </td>
   <td style="text-align:right;"> 679 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 148 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2020-05-20 </td>
   <td style="text-align:left;"> 14 </td>
   <td style="text-align:left;"> Murcia </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 1570 </td>
   <td style="text-align:right;"> 1039 </td>
   <td style="text-align:right;"> 680 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 149 </td>
  </tr>
</tbody>
</table>

Veamos la evolución diaria:


```r
p1 <- ggplot(data = covid19_rm_df, mapping = aes(x = Fecha, y = Hospitalizados)) + geom_col()
p1 + labs(caption = "Evolución diaria de hospitalizados en la Región de Murcia", 
          x = "Mes") + 
  guides(x = guide_axis(order = 1, angle = 45))
```

![plot of chunk casos](figure/casos-1.png)

Y a continuación de fallecidos:


```r
p1 <- ggplot(data = covid19_rm_df, mapping = aes(x = Fecha, y = Fallecidos)) + geom_col()
p1 + labs(caption = "Fallecimientos en la Región de Murcia", 
          x = "Mes") + 
  guides(x = guide_axis(order = 1, angle = 45))
```

![plot of chunk fallecidos](figure/fallecidos-1.png)
