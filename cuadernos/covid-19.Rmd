---
title: "Covid-19"
author: "José Miguel"
date: "31/05/2020"
output: github_document
---

# Introducción

Este [virus](https://en.wikipedia.org/wiki/Coronavirus_disease_2019) que se ha convertido en una pandemia global del siglo XXI y cuyo impacto en nuestras vidas está aún por determinar, nos servirá como excusa para ilustrar cómo usar [R](https://www.r-project.org/) para analizar y visualizar datos abiertos relativos al covid-19.

## Fuentes de datos

Agradecer el trabajo realizado por las siguientes personas y organizaciones que han facilitado datos listos para ser analizados:

- [Our World In Data](https://www.ourworldindata.org)
- [Datadista](https://github.com/datadista/datasets/tree/master/COVID%2019) para analizar por comunidad autónoma española.

## Dependencias

Para realizar las operaciones pertinentes usaremos las bibliotecas proporcionadas por [tidyverse](https://www.tidyverse.org/), que ya tenemos instaladas.

También usaremos [knitr](https://yihui.org/knitr/) para embellecer las tablas.

```{r, include=FALSE}
library(tidyverse)
library(knitr)
library(lubridate)
```

## Situación global

Importamos los datos proporcionados por Our World In Data.

```{r covid19_df, include=FALSE}
data_source_url <- "https://covid.ourworldindata.org/data/ecdc/full_data.csv"
covid19_df <- read_csv(data_source_url)
```

Para ver el dataframe, descomenta la siguiente línea:

```{r}
#covid19_df
```

> El carácter '#' se usa para indicar un comentario.

Estamos interesados en la siguiente información:

- país o *location*
- casos por día o *new_cases*
- defunciones por día o *new_deaths*

Vamos a extraer el total de casos y defunciones por país para el último día disponible. Y el último día disponible lo obtenemos de la siguiente forma:

```{r, include=FALSE}
dates <- covid19_df['date']
last_date <- dates %>%
  mutate(date = ymd(date)) %>%
  summarise(max = max(date)-1)
```
> Realmente considero el penúltimo día porque algunos países tardan en reportar :(

```{r last_day_df}
#last_day <- "2020-05-28"
last_day <- last_date$max[1]
last_day_df <- filter(covid19_df, date == last_day)
```

Seleccionemos los diez países con más casos registrados, descartando la entrada para el total mundial:

```{r top10_cases, echo = FALSE, results = 'asis'}
top_cases <- arrange(last_day_df, desc(total_cases))
top10_cases <- slice(top_cases, 2:11)
top10_cases4table <- select(top10_cases, date, location, "total_cases", "total_deaths")
kable(top10_cases4table, caption = "Top 10 countries")
```

```{r total_cases}
p <- ggplot(data = top10_cases, 
            mapping = aes(x = reorder(location, -total_cases), y = total_cases ))+ 
  geom_col()
caption <- str_interp("Total de casos a día ${last_day}")
p + guides(x = guide_axis(order = 1, angle = 45)) + 
  labs(x = "País",
       y = "Casos",
       caption = caption)
```

La segunda instrucción es para ordenar los países por número decreciente de casos.

```{r total_deaths}
p2 <- ggplot(data = top10_cases, 
             mapping = aes(x = reorder(location, -total_deaths), y = total_deaths )) + 
  geom_col()
caption <- str_interp("Total de muertes a día ${last_day}")
p2 + guides(x = guide_axis(angle = 45)) + 
  labs(x = "País",
       y = "Fallecimientos",
       caption = caption)
```

Finalmente vamos a representar el primer gráfico, asignando una escala de colores por número de fallecimientos.

```{r total_cases_deaths}
p3 <- ggplot(data = top10_cases, 
             mapping = aes(x = reorder(location, -total_deaths), 
                           y = total_cases, fill = total_deaths )) + geom_col()
caption <- str_interp("Total de casos y muertes a día ${last_day}")
p3 + guides(x = guide_axis(angle = 45)) + 
  labs(x = "País",
       y = "Casos",
       caption = caption)
```

Opcionalmente podemos guardar el gráfico generado en un fichero, pero debes descomentar el comando.

```{r}
#ggsave("top10_cases_deaths.png", width=10, height=5)
```

El problema de la última gráfica es que puede ser un poco complicada de analizar. Quizás mejor representar casos y fallecimientos en dos ejes:

```{r cases_deaths_total}
p4 <- ggplot(data = top10_cases, 
             mapping = aes(x = total_deaths, y = total_cases, label = location )) + geom_label()
caption <- str_interp("A día ${last_day}")
p4 + labs(label = "Countries", 
          x = "Fallecimientos",
          y = "Casos",
          caption = caption)
```
