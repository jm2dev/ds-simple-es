#+title: Ciencia de datos sencilla con R y Nix
#+author: José Miguel
#+startup: content

* Abstract

Simple Data Science projects in Spanish for teaching purposes with R and Nix.

* Introducción

El propósito de este repositorio es compartir una introducción sencilla a la ciencia de datos, /data science/ en inglés, usando [[https://www.r-project.org/][R]] como lenguaje y [[https://nixos.org/nix/][Nix]] como herramienta para simplificar la preparación del entorno de trabajo.

Nix, disponible para sistemas Linux, Unix, BSD y Mac OSX, es un gestor de paquetes que nos va a permitir de una manera sencilla, y sin afectar a nuestro sistema, tener instaladas rápidamente las herramientas que necesitamos para ejecutar el cuaderno o /notebook/ adjuntado.

La programación literal, o /literal programming/, resulta particularmente interesante para documentar nuestras acciones o código, y tiene un propósito didáctico que espero les resulte útil.

* Requisitos

- Nix
- terminal (xterm, urxvt, alacritty, ...): que usaremos para interactuar con Nix.

** Instalación de Nix

Es recomendable visitar su página web por si las instrucciones han cambiado, que a día de hoy se reduce a:

#+BEGIN_EXAMPLE
curl -L --proto '=https' --tlsv1.2 https://nixos.org/nix/install | sh
#+END_EXAMPLE

#+BEGIN_QUOTE
En general no es una buena práctica descargarse un script y ejecutarlo directamente, sino que se debe inspeccionar primero para comprobar que no seremos víctimas de acciones maliciosas.
#+END_QUOTE

** Uso

Si ya tenemos Nix instalado, el siguiente comando instalará localmente [[https://rstudio.com/products/rstudio/#rstudio-desktop][RStudio]] y las bibliotecas conocidas como [[https://www.tidyverse.org/][Tidyverse]], si nuestro fichero *shell.nix* contiene lo siguiente:

#+BEGIN_EXAMPLE
{ pkgs ? import <nixpkgs> {}
}:

pkgs.rstudioWrapper.override {
  packages = with pkgs.rPackages; [ tidyverse ];
}
#+END_EXAMPLE

#+BEGIN_QUOTE
El fichero *shell.nix* es leído por el comando *nix-shell*, y podemos añadir más paquetes (si están empaquetados para Nix) de forma que estarán disponibles en nuestro entorno, y no necesitaremos instalarlos desde el cuaderno. De esta forma se facilita la portabilidad de estos cuadernos.
#+END_QUOTE

#+BEGIN_EXAMPLE
nix-shell

// si todo ha ido bien
rstudio
#+END_EXAMPLE

Una vez en RStudio siéntase libre de explorar los cuadernos, ficheros con extensión /Rmd/, que encontrará en el directorio homónimo.

*** Modo batch

Con la intención de facilitar reproducir los experimentos sin abrir Rstudio, usaremos otro fichero *shell.nix* que nos creará un entorno con R y las bibliotecas necesarias para ejecutar los cuadernos:

#+BEGIN_EXAMPLE
let
  pkgs = import <nixpkgs> {};
in
  pkgs.mkShell {
    buildInputs = with pkgs; [
      R
      rPackages.rmarkdown
      rPackages.knitr
      rPackages.tidyverse
    ];
  }
#+END_EXAMPLE

Activamos el nuevo entorno:

#+BEGIN_EXAMPLE
nix-shell
#+END_EXAMPLE

Y a continuación invocaremos un script R que realizará el procesado de los cuadernos por nosotros.

#+BEGIN_EXAMPLE
make build
#+END_EXAMPLE

> NOTA: como ejemplo dejamos el fichero build.R, responsable de procesar los cuadernos, pero nótese que al invocar make se borrará y generará uno nuevo.

Finalmente comentar que personalmente uso [[https://github.com/target/lorri/][lorri]] con [[https://direnv.net/][direnv]] de una forma similar a este [[https://nixos.wiki/wiki/Development_environment_with_nix-shell][ejemplo]], y recomiendo su uso una vez familiarizado con nix-shell.

* Datos

- [[https://datadista.com/][Datadista]]
- [[https://ourworldindata.org/][Our World in Data]]

* Estructura del proyecto

R es un lenguaje de programación enfocado a análisis estádisticos. Podemos usarlo en una sesión interactiva o [[https://en.wikipedia.org/wiki/Read%25E2%2580%2593eval%25E2%2580%2593print_loop][REPL]], pero cuando el código se complica nos resultará más cómodo guardar nuestro código en un fichero con extensión *.R*.

[[https://rmarkdown.rstudio.com/][RMarkdown]] es un formato de fichero con extensión *.Rmd* donde encontramos código R con texto plano, y estos ficheros se conocen como /cuadernos/. Los cuadernos que hemos desarrollado se encuentran en el directorio homónimo.

Finalmente con la intención de facilitar su lectura, los cuadernos se han exportado a formato [[https://daringfireball.net/projects/markdown/][Markdown]] para que sean correctamente visualizados en [[https://gitlab.com/jm2dev/ds-simple-es][nuestro repositorio GitLab]].



